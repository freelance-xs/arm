<div class="bg-gray-800">
    <div>
        <nav class="bg-black">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="flex items-center justify-between h-16">
                    <p class="absolute left-4 uppercase text-white tracking-widest font-bold">ARM Partner Meeting</p>
                    <div class="flex pl-52 ">
                        <div class="hidden md:block">
                            <div class="ml-10 flex items-baseline space-x-4">
                                <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Agenda</a>

                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Sessions</a>

                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Techflix</a>

                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Speakers</a>

                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Networking</a>

                                <a href="#" class="text-white hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Podcasts</a>

                            </div>
                        </div>
                    </div>
                    <div class="hidden md:block">
                        <div class="ml-4 flex items-center md:ml-6 ">
                            <a href="#" class="text-gray-300 hover:text-white bg-red-900 px-6 py-5 text-sm font-medium uppercase"><span>NOW:</span> Live Streaming</a>

                            <!-- Profile dropdown -->
                            <div class="ml-3 absolute right-8">
                                <div class="px-8">
                                    <a href="#" class="max-w-xs underline flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" id="user-menu" aria-expanded="false" aria-haspopup="true">
                                        <img class="h-8 w-8 mx-2 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=ji810ZHyWr&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                        <p class="text-white underline">Hi, {{ optional(Auth::user())->name }}</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="-mr-2 flex md:hidden">
                        <!-- Mobile menu button -->
                        <button type="button" class="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                            <span class="sr-only">Open main menu</span>
                            <!--
                              Heroicon name: outline/menu

                              Menu open: "hidden", Menu closed: "block"
                            -->
                            <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                            </svg>
                            <!--
                              Heroicon name: outline/x

                              Menu open: "block", Menu closed: "hidden"
                            -->
                            <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Mobile menu, show/hide based on menu state. -->
            <div class="md:hidden" id="mobile-menu">
                <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                    <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                    <a href="#" class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium">Dashboard</a>

                    <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Team</a>

                    <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Projects</a>

                    <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Calendar</a>

                    <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Reports</a>
                </div>
                <div class="pt-4 pb-3 border-t border-gray-700">
                    <div class="flex items-center px-5">
                        <div class="flex-shrink-0">
                            <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=ji810ZHyWr&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                        </div>
                        <div class="ml-3">
                            <div class="text-base font-medium leading-none text-white">Tom Cook</div>
                            <div class="text-sm font-medium leading-none text-gray-400">tom@example.com</div>
                        </div>
                        <button class="ml-auto bg-gray-800 flex-shrink-0 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                            <span class="sr-only">View notifications</span>
                            <!-- Heroicon name: outline/bell -->
                            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                            </svg>
                        </button>
                    </div>
                    <div class="mt-3 px-2 space-y-1">
                        <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700">Your Profile</a>

                        <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700">Settings</a>

                        <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700">Sign out</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <main class="mt-16 mx-auto max-w-7xl px-4 sm:mt-24">

        <div class="h-screen">
            <div class="text-center pt-36">
                <h1 class="text-2xl tracking-wide uppercase">
                    <span class="block text-white">welcome to</span>
                </h1>
                <img src="{{ asset('imgs/logo.png') }}" class="mx-auto w-1/2" alt="">
                <p class="mt-3 max-w-md mx-auto text-base text-white sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">
                    Anim aute id magna aliqua ad ad non deserunt sunt. <br>Qui irure qui lorem cupidatat commodo.
                </p>
            </div>
        </div>

    </main>


    <!--
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ]
  }
  ```
-->
    <footer class="bg-gray-800" aria-labelledby="footerHeading">
        <h2 id="footerHeading" class="sr-only">Footer</h2>
        <div class="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
            <div class="pb-8 xl:grid xl:grid-cols-5 xl:gap-8">
                <div class="grid grid-cols-2 gap-8 xl:col-span-4">
                    <div class="md:grid md:grid-cols-2 md:gap-8">
                        <div>
                            <img src="{{ asset('imgs/logo.png') }}" class="w-1/10 px-8" alt="">
                        </div>
                        <div class="mt-12 md:mt-0">
                            <h3 class="text-sm font-semibold text-gray-400 tracking-wider uppercase">
                                Sitemap
                            </h3>
                            <ul class="mt-4 space-y-2">
                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Home
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Profile
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Agenda
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Sessions
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Techflix
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Speakers
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Networking
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Podcasts
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="md:grid md:grid-cols-2 md:gap-8">
                        <div>
                            <h3 class="text-sm font-semibold text-gray-400 tracking-wider uppercase">
                                Contact
                            </h3>
                            <ul class="mt-4 space-y-2">
                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        Support
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="text-base text-gray-300 hover:text-white">
                                        FAQs
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-8 border-t border-gray-700 pt-8">
                <div class="flex text-gray-400">
                    <a class="pr-3" href="#">Code of conduct</a> |
                    <a class="px-3" href="#">Cookie policy</a> |
                    <a class="px-3" href="#">Terms of Use</a> |
                    <a class="px-3" href="#">Privacy Policy</a> |
                    <a class="px-3" href="#">Accessibility</a> |
                    <a class="px-3" href="#">Trademarks</a>
                </div>
                <p class="mt-8 text-base text-gray-400">
                    Copyright 2021 &copy; Arm Limited (or its affiliates) All Rights Reserved
                </p>
            </div>
        </div>
    </footer>


</div>
