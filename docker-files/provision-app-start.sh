#!/bin/bash

# Create the pre-requisites needed for this application

touch storage/logs/laravel.log
chmod 777 -R storage/logs

chmod 777 storage/framework/sessions/

chmod 777 storage/framework/views/

mkdir -p storage/framework/cache/
chmod 777 -R storage/framework/cache/

chmod 777 -R bootstrap/cache/

# only becase this is a test
cp .env.example .env




sudo service apache2 restart
service mysql restart

# this would have kept the server running if we had passed
# in a command from the command line.
#exec "$@";

while true; do sleep 1000; done

# Keep the server running with this final command
exec /bin/bash;
